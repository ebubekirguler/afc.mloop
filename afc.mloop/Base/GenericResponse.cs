﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace afc.mloop.Base
{
    public class GenericResponse<T>
{
    public T Value;
    public bool Success
    {
        get
        {
            return Results != null && Results.Count > 0 ? false : true;
        }
    }
    public List<Result> Results { get; set; }
    public GenericResponse(T value, List<Result> result = null)
    {
        this.Value = value;
        this.Results = result ?? new List<Result>();
    }
}
}
