﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace afc.mloop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private static string InternalAPIUri { get; set; }
        private IConfiguration _configuration;
        private readonly ILogger<TicketController> _logger;

        public TicketController(IConfiguration Configuration, ILogger<TicketController> logger)
        {
            Utility.Util._configuration = _configuration = Configuration;
            InternalAPIUri = Configuration.GetSection("InternalAPI").GetSection("APIUrl").Value;
            _logger = logger;

        }
        [Route("all")]
        [HttpGet]
        public ActionResult GetAll([FromHeader(Name = "Authorization")] string authorizationHeader)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var path = _configuration.GetSection("InternalAPI").GetSection("TicketGetAll").Value;
                var response = Utility.Util.ProcessGet(Request, $"{InternalAPIUri}{path}");
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);

                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
        [Route("byId")]
        [HttpGet]
        public ActionResult GetById([FromHeader(Name = "Authorization")] string authorizationHeader, [FromQuery]int id)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var path = _configuration.GetSection("InternalAPI").GetSection("TicketGet").Value;
                var response = Utility.Util.ProcessGet(Request, $"{InternalAPIUri}{path}/{id}");
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);

                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
    }
}