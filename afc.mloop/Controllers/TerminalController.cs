﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace afc.mloop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TerminalController : ControllerBase
    {
        private static string InternalAPIUri { get; set; }
        private IConfiguration _configuration;
        private readonly ILogger<TerminalController> _logger;

        public TerminalController(IConfiguration Configuration, ILogger<TerminalController> logger)
        {
            Utility.Util._configuration = _configuration = Configuration;
            InternalAPIUri = Configuration.GetSection("InternalAPI").GetSection("APIUrl").Value;
            _logger = logger;

        }
        [Route("all")]
        [HttpGet]
        public ActionResult GetAll([FromHeader(Name = "Authorization")] string authorizationHeader)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var path = _configuration.GetSection("InternalAPI").GetSection("TerminalGetAll").Value;
                var response = Utility.Util.ProcessGet(Request, $"{InternalAPIUri}{path}");
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);

                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
    }
}