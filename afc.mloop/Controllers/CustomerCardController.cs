﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using afc.mloop.Base;
using afc.mloop.Models;
using afc.mloop.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;

namespace afc.mloop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerCardController : ControllerBase
    {
        private IConfiguration _configuration;
        private readonly ILogger<CustomerCardController> _logger;

        public CustomerCardController(IConfiguration Configuration, ILogger<CustomerCardController> logger)
        {
            Util._configuration = _configuration = Configuration;
            _logger = logger;

        }
        [HttpPost]
        [Route("create")]
        /// <param name="user">user</param>
        public ActionResult Create([FromHeader(Name = "Authorization")] string authorizationHeader, CustomerCardEntity body)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var customerId = (int)HttpContext.Items["customer_id"];
                var newBody = new CustomerCardInternalEntity()
                {
                    CustomerId = customerId,
                    CardNumber = body.CardNumber,
                    UId = body.UId
                };
                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerCardCreate").Value;
                var json = JsonConvert.SerializeObject(newBody);
                byte[] byteArray = Encoding.ASCII.GetBytes(json);
                MemoryStream stream = new MemoryStream(byteArray);
                Request.Body = stream;
                var response = Util.ProcessPost(Request, $"{Util.InternalAPIUri}{path}", out string content);
                _logger.LogInformation($"Reqeust: {operationId} Req Content: " + content);
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);

                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }


        }
        [HttpPost]
        [Route("delete")]
        /// <param name="user">user</param>
        public ActionResult Delete([FromHeader(Name = "Authorization")] string authorizationHeader, CustomerCardEntity body)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var customerId = (int)HttpContext.Items["customer_id"];
                var newBody = new CustomerCardInternalEntity()
                {
                    CustomerId = customerId,
                    Id = body.Id
                };
                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerCardDelete").Value;
                var json = JsonConvert.SerializeObject(newBody);
                byte[] byteArray = Encoding.ASCII.GetBytes(json);
                MemoryStream stream = new MemoryStream(byteArray);
                Request.Body = stream;
                var response = Util.ProcessPost(Request, $"{Util.InternalAPIUri}{path}", out string content);
                _logger.LogInformation($"Reqeust: {operationId} Req Content: " + content);
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);

                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }


        }
        [HttpGet]
        [Route("get")]
        public ActionResult Get([FromHeader(Name = "Authorization")] string authorizationHeader)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var customerId = (int)HttpContext.Items["customer_id"];
                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerCardGet").Value;
                var response = Util.ProcessGet(Request, $"{Util.InternalAPIUri}{path}/{customerId}");
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);
                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
        [HttpPost]
        [Route("TopUp")]
        /// <param name="user">user</param>
        public ActionResult TopUp([FromHeader(Name = "Authorization")] string authorizationHeader, CustomerCardTopUpAPIEntity body)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var customerId = (int)HttpContext.Items["customer_id"];
                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerCardTopUp").Value;
                var json = JsonConvert.SerializeObject(body);
                byte[] byteArray = Encoding.ASCII.GetBytes(json);
                MemoryStream stream = new MemoryStream(byteArray);
                Request.Body = stream;
                var response = Util.ProcessPost(Request, $"{Util.InternalAPIUri}{path}", out string content);
                _logger.LogInformation($"Reqeust: {operationId} Req Content: " + content);
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);

                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }


        }
        [HttpPost]
        [Route("GetTransaction")]
        /// <param name="user">user</param>
        public ActionResult GetTransaction([FromHeader(Name = "Authorization")] string authorizationHeader, CustomerCardEntity body)
        {
            var operationId = Guid.NewGuid();
            try
            {
                body.CustomerId = (int)HttpContext.Items["customer_id"];
                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerCardTransaction").Value;
                var json = JsonConvert.SerializeObject(body);
                byte[] byteArray = Encoding.ASCII.GetBytes(json);
                MemoryStream stream = new MemoryStream(byteArray);
                Request.Body = stream;
                var response = Util.ProcessPost(Request, $"{Util.InternalAPIUri}{path}", out string content);
                _logger.LogInformation($"Reqeust: {operationId} Req Content: " + content);
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);

                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }


        }
        [HttpPost]
        [Route("AddCard")]
        /// <param name="user">user</param>
        public ActionResult AddCard([FromHeader(Name = "Authorization")] string authorizationHeader, CustomerCardEntity body)
        {
            var operationId = Guid.NewGuid();
            try
            {
                body.CustomerId = (int)HttpContext.Items["customer_id"];
                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerCardAddCard").Value;
                var json = JsonConvert.SerializeObject(body);
                byte[] byteArray = Encoding.ASCII.GetBytes(json);
                MemoryStream stream = new MemoryStream(byteArray);
                Request.Body = stream;
                var response = Util.ProcessPost(Request, $"{Util.InternalAPIUri}{path}", out string content);
                _logger.LogInformation($"Reqeust: {operationId} Req Content: " + content);
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);

                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }


        }


    }
}