﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using afc.mloop.Base;
using afc.mloop.Models;
using afc.mloop.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;

namespace afc.mloop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private IConfiguration _configuration;
        private readonly ILogger<CustomerController> _logger;

        public CustomerController(IConfiguration Configuration, ILogger<CustomerController> logger)
        {
            Util._configuration = _configuration = Configuration;
            _logger = logger;

        }
        [HttpPost]
        [Route("create")]
        /// <param name="user">user</param>
        public ActionResult Create(User body)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerCreate").Value;
                var json = JsonConvert.SerializeObject(body);
                byte[] byteArray = Encoding.ASCII.GetBytes(json);
                MemoryStream stream = new MemoryStream(byteArray);
                Request.Body = stream;
                var response = Util.ProcessPost(Request, $"{Util.InternalAPIUri}{path}", out string content);
                _logger.LogInformation($"Reqeust: {operationId} Req Content: " + content);
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);

                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }


        }
        [HttpPost]
        [Route("update")]
        public ActionResult Update([FromHeader(Name = "Authorization")] string authorizationHeader, UpdateUserEntity body)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var json = JsonConvert.SerializeObject(body);
                byte[] byteArray = Encoding.ASCII.GetBytes(json);
                MemoryStream stream = new MemoryStream(byteArray);
                Request.Body = stream;

                var customerId = (int)HttpContext.Items["customer_id"];
                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerUpdate").Value;
                var response = Util.ProcessPost(Request, $"{Util.InternalAPIUri}{path}/{customerId}", out string content);
                _logger.LogInformation($"Reqeust: {operationId} Req Content: " + content);
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);
                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
        [HttpGet]
        [Route("get")]
        public ActionResult Get([FromHeader(Name = "Authorization")] string authorizationHeader)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var customerId = (int)HttpContext.Items["customer_id"];
                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerGet").Value;
                var response = Util.ProcessGet(Request, $"{Util.InternalAPIUri}{path}/{customerId}");
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);
                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
        [HttpPost]
        [Route("forgotPassword")]
        public ActionResult ForgotPassword(ForgotPasswordDemandEntity body)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var json = JsonConvert.SerializeObject(body);
                byte[] byteArray = Encoding.ASCII.GetBytes(json);
                MemoryStream stream = new MemoryStream(byteArray);
                Request.Body = stream;
                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerForgotPasswordDemand").Value;
                var response = Util.ProcessPost(Request, $"{Util.InternalAPIUri}{path}", out string content);
                _logger.LogInformation($"Reqeust: {operationId} Req Content: " + content);
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);
                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }

        }
        [HttpPost]
        [Route("forgotPassword/validate")]
        public ActionResult ForgotPasswordDemandValidate(ValidateForgotPasswordDemandEntity body)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var json = JsonConvert.SerializeObject(body);
                byte[] byteArray = Encoding.ASCII.GetBytes(json);
                MemoryStream stream = new MemoryStream(byteArray);
                Request.Body = stream;
                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerForgotPasswordDemandValidate").Value;
                var response = Util.ProcessPost(Request, $"{Util.InternalAPIUri}{path}", out string content);
                _logger.LogInformation($"Reqeust: {operationId} Req Content: " + content);
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);
                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
    }
}