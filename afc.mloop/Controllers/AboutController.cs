﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using afc.mloop.Base;
using afc.mloop.Models;
using afc.mloop.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace afc.mloop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AboutController : Controller
    {
        private IConfiguration _configuration;

        public AboutController(IConfiguration Configuration)
        {
            Util._configuration = _configuration = Configuration;

        }
        [HttpGet]
        [Route("get")]
        public IActionResult Get()
        {
            var path = _configuration.GetSection("InternalAPI").GetSection("NewsGet").Value;
            var listJson = Util.ProcessGet(Request, $"{Util.InternalAPIUri}{path}");
            var listdynamic = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(listJson);
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NewsEntity>>(listdynamic.Value.ToString());
            ViewBag.List = list;
            return View("~/Views/About/Index.cshtml");
        }
        [HttpGet]
        [Route("getAll")]
        public IActionResult GetAll()
        {
            var path = _configuration.GetSection("InternalAPI").GetSection("NewsGet").Value;
            var listJson = Util.ProcessGet(Request, $"{Util.InternalAPIUri}{path}");
            return Content(listJson, "application/json");

        }
    }
}