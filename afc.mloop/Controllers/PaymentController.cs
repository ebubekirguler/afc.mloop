﻿
using afc.mloop.Base;
using afc.mloop.Models;
using afc.mloop.Utility;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace afc.mloop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : Controller
    {
        private static string InternalAPIUri { get; set; }
        private IConfiguration _configuration;
        private readonly ILogger<PaymentController> _logger;

        public PaymentController(IConfiguration Configuration, ILogger<PaymentController> logger)
        {
            Util._configuration = _configuration = Configuration;
            InternalAPIUri = Configuration.GetSection("InternalAPI").GetSection("APIUrl").Value;
            _logger = logger;

        }
        [Route("secure")]
        [HttpGet]
        public IActionResult Payment([FromQuery]string token, [FromQuery]int ticketDefinitionId, [FromQuery]int quantity, [FromQuery]string lang)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var isTokenValid = HttpContext.Items.ContainsKey("token_valid") ? (bool)HttpContext.Items["token_valid"] : true;
                if (!isTokenValid)
                {
                    return View("~/Views/Payment/InvalidToken.cshtml");

                }
                if (HttpContext.Request.Headers.ContainsKey("Accept-Language"))
                    HttpContext.Request.Headers.Remove("Accept-Language");
                HttpContext.Request.Headers.Add("Accept-Language", lang);
                #region GetTicketDetail
                var path = _configuration.GetSection("InternalAPI").GetSection("TicketGet").Value;
                var ticketDefinitionResponse = Util.ProcessGet(Request, $"{Util.InternalAPIUri}{path}/{ticketDefinitionId}");
                var ticketDefinition = Newtonsoft.Json.JsonConvert.DeserializeObject<GenericResponse<TicketDefinitionEntity>>(ticketDefinitionResponse);
                if (!ticketDefinition.Success)
                    return Content(ticketDefinitionResponse, "application/json");
                #endregion
                ViewBag.Token = token;
                ViewBag.TicketCost = ticketDefinition.Value.TicketCost * quantity;
                ViewBag.TicketName = ticketDefinition.Value.Name;
                ViewBag.Quantity = quantity;
                ViewBag.TicketDefinitionId = ticketDefinitionId;
                ViewBag.Lang = lang;
                _logger.LogInformation($"Reqeust: {operationId} Query: token:{token} ticketDefinitionId:{ticketDefinitionId} quantity:{quantity} lang:{lang} ");

                return View("~/Views/Payment/Index.cshtml");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
        [Route("recharge")]
        [HttpGet]
        public IActionResult Recharge([FromQuery]string token, [FromQuery]string cardNumber, [FromQuery]string cardUid)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var isTokenValid = HttpContext.Items.ContainsKey("token_valid") ? (bool)HttpContext.Items["token_valid"] : true;
                if (!isTokenValid)
                {
                    return View("~/Views/Payment/InvalidToken.cshtml");

                }
                if (HttpContext.Request.Headers.ContainsKey("Accept-Language"))
                    HttpContext.Request.Headers.Remove("Accept-Language");
                HttpContext.Request.Headers.Add("Accept-Language", "en");

                ViewBag.Token = token;
                ViewBag.Lang = "en";
                ViewBag.CardNumber = cardNumber;
                ViewBag.Uid = cardUid;

                return View("~/Views/Recharge/Index.cshtml");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
        [Route("doRecharge")]
        [HttpPost]
        public IActionResult doRecharge([FromBody]object formdatapair)
        {
            var operationId = Guid.NewGuid();
            try
            {
                _logger.LogInformation($"Reqeust: {operationId} Query: formdatapair:{formdatapair}");

                var datas = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FormDataNameValue>>(formdatapair.ToString());
                var token = datas.First(i => i.name == "Token").value;
                if (!Util.CheckToken("Bearer " + token))
                {
                    return View("~/Views/Payment/InvalidToken.cshtml");
                }
                var amount = int.Parse(datas.First(i => i.name == "amount").value);
                var uid = datas.First(i => i.name == "uid").value;
                var customerId = (int)HttpContext.Items["customer_id"];
                var lang = "en";

                var recharge = new CustomerCardTopUpAPIEntity()
                {
                    Amount = amount,
                    CustomerId = customerId,
                    UId = uid,
                    Date = DateTime.Now
                };
                var buyResponse = RechargeCard(recharge, lang);
                return Content(buyResponse.Content, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
        [Route("do")]
        [HttpPost]
        public IActionResult Do([FromBody]object formdatapair)
        {
            var operationId = Guid.NewGuid();
            try
            {
                _logger.LogInformation($"Reqeust: {operationId} Query: formdatapair:{formdatapair}");

                var datas = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FormDataNameValue>>(formdatapair.ToString());
                var token = datas.First(i => i.name == "Token").value;
                if (!Util.CheckToken("Bearer " + token))
                {
                    return View("~/Views/Payment/InvalidToken.cshtml");
                }
                var ticketDefinitionId = int.Parse(datas.First(i => i.name == "TicketDefinitionId").value);
                var quantity = int.Parse(datas.First(i => i.name == "Quantity").value);
                var customerId = (int)HttpContext.Items["customer_id"];
                var lang = datas.First(i => i.name == "Lang").value;
                #region GetTicketDetail

                var path = _configuration.GetSection("InternalAPI").GetSection("TicketGet").Value;
                var ticketDefinitionResponse = Util.ProcessGet(Request, $"{Util.InternalAPIUri}{path}/{ticketDefinitionId}");
                var ticketDefinition = Newtonsoft.Json.JsonConvert.DeserializeObject<GenericResponse<TicketDefinitionEntity>>(ticketDefinitionResponse);
                if (!ticketDefinition.Success)
                    return Content(ticketDefinitionResponse, "application/json");
                #endregion
                var ticketBuy = new CustomerTicketEntity()
                {
                    TicketDefinitionId = ticketDefinitionId,
                    TicketCost = ticketDefinition.Value.TicketCost.Value * quantity,
                    CustomerId = customerId,
                    TicketCount = quantity
                };
                var buyResponse = BuyTicket(ticketBuy, lang);
                return Content(buyResponse.Content, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
        [Route("purchaseTicket")]
        [HttpPost]
        public IActionResult DoExternal([FromHeader(Name = "Authorization")] string authorizationHeader, [FromBody]PaymentEntity body)
        {
            var operationId = Guid.NewGuid();
            try
            {
                _logger.LogInformation($"Reqeust: {operationId} Query: formdatapair:{Newtonsoft.Json.JsonConvert.SerializeObject(body)}");
                var ticketDefinitionId = body.TicketDefinitionId;
                var quantity = body.Quantity;
                var customerId = (int)HttpContext.Items["customer_id"];
                var lang = body.Lang;
                #region GetTicketDetail

                var path = _configuration.GetSection("InternalAPI").GetSection("TicketGet").Value;
                var ticketDefinitionResponse = Util.ProcessGet(Request, $"{Util.InternalAPIUri}{path}/{ticketDefinitionId}");
                var ticketDefinition = Newtonsoft.Json.JsonConvert.DeserializeObject<GenericResponse<TicketDefinitionEntity>>(ticketDefinitionResponse);
                if (!ticketDefinition.Success)
                    return Content(ticketDefinitionResponse, "application/json");
                #endregion
                var ticketBuy = new CustomerTicketEntity()
                {
                    TicketDefinitionId = ticketDefinitionId,
                    TicketCost = ticketDefinition.Value.TicketCost.Value * quantity,
                    CustomerId = customerId,
                    TicketCount = quantity
                };
                var buyResponse = BuyTicket(ticketBuy, lang);
                return Content(buyResponse.Content, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
        [Route("addPaymentMethod")]
        [HttpPost]
        public IActionResult AddPaymentMethod([FromHeader(Name = "Authorization")] string authorizationHeader, [FromBody]PaymentMethodEntity body)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var customerId = (int)HttpContext.Items["customer_id"];
                body.CustomerId = customerId;
                var path = _configuration.GetSection("InternalAPI").GetSection("PaymentMethodCreate").Value;
                var json = JsonConvert.SerializeObject(body);
                byte[] byteArray = Encoding.ASCII.GetBytes(json);
                MemoryStream stream = new MemoryStream(byteArray);
                Request.Body = stream;
                var response = Util.ProcessPost(Request, $"{Util.InternalAPIUri}{path}", out string content);
                _logger.LogInformation($"Reqeust: {operationId} Req Content: " + content);
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);

                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }

        }
        [Route("deletePaymentMethod")]
        [HttpPost]
        public IActionResult DeletePaymentMethod([FromHeader(Name = "Authorization")] string authorizationHeader, [FromBody]PaymentMethodEntity body)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var customerId = (int)HttpContext.Items["customer_id"];
                body.CustomerId = customerId;
                var path = _configuration.GetSection("InternalAPI").GetSection("PaymentMethodDelete").Value;
                var json = JsonConvert.SerializeObject(body);
                byte[] byteArray = Encoding.ASCII.GetBytes(json);
                MemoryStream stream = new MemoryStream(byteArray);
                Request.Body = stream;
                var response = Util.ProcessPost(Request, $"{Util.InternalAPIUri}{path}", out string content);
                _logger.LogInformation($"Reqeust: {operationId} Req Content: " + content);
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);

                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }

        }
        [Route("getCustomerPaymentMethod")]
        [HttpGet]
        public ActionResult GetCustomerPaymentMethod([FromHeader(Name = "Authorization")] string authorizationHeader)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var customerId = (int)HttpContext.Items["customer_id"];
                var path = _configuration.GetSection("InternalAPI").GetSection("PaymentMethodByCustomer").Value;
                var response = Utility.Util.ProcessGet(Request, $"{InternalAPIUri}{path}/{customerId}");
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);

                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
        private IRestResponse BuyTicket(CustomerTicketEntity item, string lang)
        {
            var path = _configuration.GetSection("InternalAPI").GetSection("TicketBuy").Value;
            var client = new RestClient($"{InternalAPIUri}{path}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept-Language", lang.ToString());
            request.AddHeader("Token", Util.ApiAuthCode);
            request.AddParameter("undefined", Newtonsoft.Json.JsonConvert.SerializeObject(item), ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var loginResponse = Util.ProcessLogin();
                if (loginResponse.StatusCode != System.Net.HttpStatusCode.OK)
                    return loginResponse;
                return BuyTicket(item, lang);
            }
            return response;
        }
        private IRestResponse RechargeCard(CustomerCardTopUpAPIEntity item, string lang)
        {
            var path = _configuration.GetSection("InternalAPI").GetSection("CustomerCardTopUp").Value;
            var client = new RestClient($"{InternalAPIUri}{path}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept-Language", lang.ToString());
            request.AddHeader("Token", Util.ApiAuthCode);
            request.AddParameter("undefined", Newtonsoft.Json.JsonConvert.SerializeObject(item), ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var loginResponse = Util.ProcessLogin();
                if (loginResponse.StatusCode != System.Net.HttpStatusCode.OK)
                    return loginResponse;
                return RechargeCard(item, lang);
            }
            return response;
        }
    }
    public class FormDataNameValue
    {
        public string name { get; set; }
        public string value { get; set; }
    }

}