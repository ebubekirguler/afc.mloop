﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using afc.mloop.Base;
using afc.mloop.Models;
using afc.mloop.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;

namespace afc.mloop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerTicketController : ControllerBase
    {
        private static string InternalAPIUri { get; set; }
        private IConfiguration _configuration;
        private readonly ILogger<CustomerTicketController> _logger;

        public CustomerTicketController(IConfiguration Configuration, ILogger<CustomerTicketController> logger)
        {
            Utility.Util._configuration = _configuration = Configuration;
            InternalAPIUri = Configuration.GetSection("InternalAPI").GetSection("APIUrl").Value;
            _logger = logger;

        }
        [Route("byCustomer")]
        [HttpGet]
        public ActionResult GetByCustomer([FromHeader(Name = "Authorization")] string authorizationHeader)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var customerId = (int)HttpContext.Items["customer_id"];
                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerTicketByCustomer").Value;
                var response = Utility.Util.ProcessGet(Request, $"{InternalAPIUri}{path}/{customerId}");
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);

                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
        [Route("validateTicket")]
        [HttpPost]
        public ActionResult UseTicket([FromHeader(Name = "Authorization")] string authorizationHeader, TicketUseEntity body)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var json = JsonConvert.SerializeObject(body);
                byte[] byteArray = Encoding.ASCII.GetBytes(json);
                MemoryStream stream = new MemoryStream(byteArray);
                Request.Body = stream;

                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerTicketUse").Value;
                var response = Util.ProcessPost(Request, $"{Util.InternalAPIUri}{path}", out string content);
                _logger.LogInformation($"Reqeust: {operationId} Req Content: " + content);
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);
                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
        [Route("validateExit")]
        [HttpPost]
        public ActionResult ValidateExit([FromHeader(Name = "Authorization")] string authorizationHeader, ValidateExitEntity body)
        {
            var operationId = Guid.NewGuid();
            try
            {
                var json = JsonConvert.SerializeObject(body);
                byte[] byteArray = Encoding.ASCII.GetBytes(json);
                MemoryStream stream = new MemoryStream(byteArray);
                Request.Body = stream;

                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerValidateExit").Value;
                var response = Util.ProcessPost(Request, $"{Util.InternalAPIUri}{path}", out string content);
                _logger.LogInformation($"Reqeust: {operationId} Req Content: " + content);
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);
                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
        [Route("validateQr")]
        [HttpPost]
        public ActionResult ValidateQr([FromHeader(Name = "Authorization")] string authorizationHeader)
        {
            var operationId = Guid.NewGuid();
            try
            {


                Request.EnableRewind();

                var buffer = new byte[Convert.ToInt32(Request.ContentLength)];

                Request.Body.ReadAsync(buffer, 0, buffer.Length);

                var bodyAsText = Encoding.UTF8.GetString(buffer);
                var json = $"{{ \"TicketNumber\":\"{bodyAsText}\" }}";

                byte[] byteArray = Encoding.ASCII.GetBytes(json);
                MemoryStream stream = new MemoryStream(byteArray);
                Request.Body = stream;

                var path = _configuration.GetSection("InternalAPI").GetSection("CustomerTicketQrValidate").Value;
                var response = Util.ProcessPost(Request, $"{Util.InternalAPIUri}{path}", out string content);
                _logger.LogInformation($"Reqeust: {operationId} Req Content: " + content);
                _logger.LogInformation($"Reqeust: {operationId} Res Content" + response);
                return Content(response, "application/json");
            }
            catch (Exception e)
            {
                _logger.LogError($"Reqeust: {operationId} Error: " + e.ToString());
                return Content("Error!", "application/json");
            }
        }
    }
}