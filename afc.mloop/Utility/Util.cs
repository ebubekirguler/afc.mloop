﻿using afc.mloop.Base;
using afc.mloop.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace afc.mloop.Utility
{
    public class Util
    {
        public static IConfiguration _configuration;
        public static string InternalAPIUri { get; set; }
        public static string IDServerUrl { get; set; }
        public static string ApiAuthUrl { get; set; }
        public static string ApiUserName { get; set; }
        public static string ApiPassword { get; set; }
        public static string ApiAuthCode { get; set; }
        public static string ProcessGet(HttpRequest Req, string uri)
        {
            var language = Req.GetTypedHeaders()?.AcceptLanguage?.FirstOrDefault();
            var lang = language != null ? language.ToString() : _configuration.GetSection("Language").GetSection("Default").Value;
            var client = new RestClient(uri);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept-Language", lang);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Token", ApiAuthCode);
            IRestResponse response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var loginResponse = ProcessLogin();
                if (loginResponse.StatusCode != System.Net.HttpStatusCode.OK)
                    return MakeResponse(loginResponse.StatusCode.ToString());
                return ProcessGet(Req, uri);
            }
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
                return MakeResponse(response.StatusCode.ToString());
            return response.Content;
        }
        public static string ProcessPost(HttpRequest Req, string uri, out string requestContent, bool isContentActive = false, string activeContent = "")
        {
            var language = Req.GetTypedHeaders()?.AcceptLanguage?.FirstOrDefault();
            var lang = language != null ? language.ToString() : _configuration.GetSection("Language").GetSection("Default").Value;
            var content = requestContent = activeContent;
            if (!isContentActive)
                content = requestContent = FormatRequest(Req);
            var client = new RestClient(uri);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept-Language", lang);
            request.AddHeader("Token", ApiAuthCode);
            request.AddParameter("entity", content, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var loginResponse = ProcessLogin();
                if (loginResponse.StatusCode != System.Net.HttpStatusCode.OK)
                    return MakeResponse(loginResponse.StatusCode.ToString());
                return ProcessPost(Req, uri, out var cont, true, requestContent);
            }
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var loginResponse = ProcessLogin();
                if (loginResponse.StatusCode != System.Net.HttpStatusCode.OK)
                    return MakeResponse(loginResponse.StatusCode.ToString());
            }
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
                return MakeResponse(response.StatusCode.ToString());
            return response.Content;

        }
        public static bool CheckToken(string authorization)
        {
            var client = new RestClient($"{IDServerUrl}{ApiAuthUrl}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", authorization);
            request.AddHeader("Content-Type", "application/json");
            IRestResponse response = client.Execute(request);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return false;
            }
            var returnResponse = JsonConvert.DeserializeObject<GenericResponse<Token>>(response.Content);
            if (!returnResponse.Success)
            {
                return false;
            }
            return true;
        }
        public static IRestResponse ProcessLogin()
        {
            var byteArray = Encoding.ASCII.GetBytes(ApiUserName + ":" + ApiPassword);

            var client = new RestClient($"{Util.InternalAPIUri}login");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", $"Basic {Convert.ToBase64String(byteArray)}");
            request.AddHeader("Content-Type", "application/json");
            IRestResponse response = client.Execute(request);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return response;
            }
            GenerateAuthCode(response);
            return response;
        }
        private static string FormatRequest(HttpRequest request)
        {
            var body = request.Body;

            request.EnableRewind();

            var buffer = new byte[Convert.ToInt32(request.Body.Length)];

            request.Body.ReadAsync(buffer, 0, buffer.Length);

            var bodyAsText = Encoding.UTF8.GetString(buffer);
            request.Body = body;

            return bodyAsText;
        }
        private static string MakeResponse(string errorCode)
        {
            var response = new GenericResponse<string>(string.Empty);
            response.Results.Add(new Result()
            {
                ErrorCode = errorCode
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(response);
        }
        private static void GenerateAuthCode(IRestResponse response)
        {
            string authToken = response.Headers.First(i => i.Name == "Token").Value.ToString();
            var userInfo = response.Headers.First(i => i.Name == "UserInfo").Value.ToString();

            var user = Newtonsoft.Json.JsonConvert.DeserializeObject<UserEntity>(userInfo);
            string authCode = "";
            string dateTime = GetLongCurrentDateTime().ToString();
            int checkSum = 0;

            authCode = user.Id.ToString("D12") + authToken.Substring(0, 8) + dateTime;
            checkSum = (int)authCode[0] + (int)authCode[6] + (int)authCode[12] + (int)authCode[18] + (int)authCode[24];
            checkSum = checkSum % 99;
            authCode += checkSum.ToString("D2");

            ApiAuthCode = EncodeString(authCode);

            return;
        }
        public static Int64 GetLongCurrentDateTime()
        {
            DateTime dtNow = DateTime.UtcNow;
            Int64 lDtNow = -1;
            String strDtNow = dtNow.ToString("yyyyMMddHHmmss");
            Int64.TryParse(strDtNow, out lDtNow);
            return lDtNow;
        }
        public static string EncodeString(String rawString)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(rawString);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string SplitAuthorizationHeader(string authorization)
        {
            return authorization.Split(' ')[1];
        }
    }
}
