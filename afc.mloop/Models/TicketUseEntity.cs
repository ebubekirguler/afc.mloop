﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace afc.mloop.Models
{
    public class QREntity
    {
        public string TicketNumber { get; set; }
    }
    public class TicketUseEntity
    {
        public string UId { get; set; }
        public decimal FareAmount { get; set; }
        public long? CustomerCardId { get; set; }
        public string TicketNumber { get; set; }
        public string DeviceGivenNumber { get; set; }
        public string ValidationDateTime { get; set; }
        public string ValidationLongitude { get; set; }
        public string ValidationLatitude { get; set; }
    }
    public class ValidateExitEntity
    {
        public long Id { get; set; }
        public string ExitValidationDateTime { get; set; }
        public string ExitValidationLongitude { get; set; }
        public string ExitValidationLatitude { get; set; }
    }
}
