﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace afc.mloop.Models
{
    public class PaymentMethodEntity
    {
        public long Id { get; set; }
        public long? CustomerId { get; set; }
        public string CardNumber { get; set; }
        public int? ExpMonth { get; set; }
        public int? ExpYear { get; set; }
        public string NameOnCard { get; set; }
        public int? CVV { get; set; }
        public string ZipCode { get; set; }
    }
}
