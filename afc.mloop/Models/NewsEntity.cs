﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace afc.mloop.Models
{
    public class NewsEntity
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string Link { get; set; }
        public string LinkDesc { get; set; }
    }
}
