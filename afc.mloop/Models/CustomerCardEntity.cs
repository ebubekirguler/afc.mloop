﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace afc.mloop.Models
{
    public class CustomerCardTopUpAPIEntity
    {
        public string UId { get; set; }
        public decimal? Amount { get; set; }
        public string DeviceGivenNumber { get; set; }
        public DateTime Date { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public long CustomerId { get; set; }

    }
    public class CustomerCardEntity
    {
        public long Id { get; set; }
        public string CardNumber { get; set; }
        public string UId { get; set; }
        public long CustomerId { get; set; }
    }
    public class CustomerCardInternalEntity
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public string CardNumber { get; set; }
        public string UId { get; set; }
    }
}
