﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace afc.mloop.Models
{
    public class UserEntity
    {
        public long Id { get; set; }
    }
    public class User
    {
        public string FullName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
    public class UpdateUserEntity : User
    {
        public string NewPassword { get; set; }
    }
}
