﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace afc.mloop.Models
{
    public class CardRechargeEntity
    {
        public long CustomerId { get; set; }
        public decimal Amount { get; set; }
    }
}
