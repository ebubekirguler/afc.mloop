﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace afc.mloop.Models
{
    public class TicketDefinitionEntity
    {
        public long Id { get; set; }

        public long? TicketTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte? UsageLimit { get; set; }
        public decimal? TicketCost { get; set; }
        public string TicketNumber { get; set; }
    }
}
