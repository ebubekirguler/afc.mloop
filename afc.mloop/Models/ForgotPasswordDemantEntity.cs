﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace afc.mloop.Models
{
    public class ForgotPasswordDemandEntity
    {
        public string Email { get; set; }
    }
    public class ValidateForgotPasswordDemandEntity : ForgotPasswordDemandEntity
    {
        public string OTP { get; set; }
        public string Password { get; set; }
    }
}
