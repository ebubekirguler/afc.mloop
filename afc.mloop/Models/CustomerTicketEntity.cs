﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace afc.mloop.Models
{
    public class CustomerTicketEntity
    {
        public long CustomerId { get; set; }
        public long TicketDefinitionId { get; set; }
        public decimal TicketCost { get; set; }
        public int TicketCount { get; set; }
    }
}
