﻿using afc.mloop.Base;
using afc.mloop.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace afc.mloop.Middleware
{

    public class AuthorizationMiddleware
    {
        private readonly RequestDelegate _next;

        private readonly ILogger<AuthorizationMiddleware> _logger;
        public AuthorizationMiddleware(RequestDelegate next, ILogger<AuthorizationMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            var isPayment = false;
            if (context.Request.Path.ToString().ToLower().Contains("forgotpassword")
                || context.Request.Path.ToString().ToLower().Contains("customer/create")
                || context.Request.Path.ToString().ToLower().Contains("files")
                || context.Request.Path.ToString().ToLower().Contains("about")
                || context.Request.Path.ToString().ToLower().Contains("swagger"))
                await _next(context);
            else
            {
                var authorization = "";

                if (context.Request.Path.ToString().ToLower().Contains("payment/secure")
                    || context.Request.Path.ToString().ToLower().Contains("payment/recharge"))
                {
                    if (!context.Request.Query.ContainsKey("token"))
                    {
                        context.Response.StatusCode = 400;
                        return;
                    }
                    authorization = $"Bearer { context.Request.Query["token"].ToString()}";
                    isPayment = true;
                }
                else if (!context.Request.Headers.ContainsKey("Authorization"))
                {
                    context.Response.StatusCode = 400;
                    return;
                }
                authorization = authorization == "" ? context.Request.Headers["Authorization"].ToString() : authorization;

                var client = new RestClient($"{Util.IDServerUrl}{Util.ApiAuthUrl}");
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", authorization);
                request.AddHeader("Content-Type", "application/json");
                IRestResponse response = client.Execute(request);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    context.Response.StatusCode = 401;
                    return;
                }
                try
                {
                    var returnResponse = JsonConvert.DeserializeObject<GenericResponse<Token>>(response.Content);
                    if (!returnResponse.Success)
                    {
                        if (isPayment)
                        {
                            context.Items.Add("token_valid", false);
                            await _next(context);
                        }
                        else
                        {
                            context.Response.StatusCode = 401;
                            return;
                        }
                    }
                    context.Items.Add("customer_id", returnResponse.Value.customer_id);
                }
                catch (Exception e)
                {
                    _logger.LogError($"Error on auth middleware {authorization}" + e.ToString());
                    return;
                }


                await _next(context);
            }


        }
    }
}
